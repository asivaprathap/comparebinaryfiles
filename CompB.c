#include<string.h>
#include<stdio.h>
#include<time.h>

clock_t start, end;
#define CMP_N 256
#define READSZ 16

int compare_two_binary_files(FILE *fp1, FILE *fp2, unsigned int *offset) {
	char tmp1[CMP_N], tmp2[CMP_N];
	size_t n1, n2;

	rewind(fp1);  // start at beginning and clear error
	rewind(fp2);
	*offset = 0;
	do {
		n1 = fread(tmp1, sizeof *tmp1, sizeof tmp1 / sizeof *tmp1, fp1);
		if (n1 == 0 && ferror(fp1)) {
			return -1;
		}
		n2 = fread(tmp2, sizeof *tmp2, sizeof tmp2 / sizeof *tmp2, fp2);
		if (n2 == 0 && ferror(fp2)) {
			return -2;
		}
		size_t n_min = n1 < n2 ? n1 : n2;
		if (memcmp(tmp1, tmp2, n_min)) {        // Quickly find if file contents differ ...
			for (size_t i = 0; i < n_min; i++) {  // Slowly find where they differ
				if (tmp1[i] != tmp2[i]) {
					*offset += i;
					return 3;
                         	}
                	}
		}
		*offset += n_min;
		if (n1 > n_min) {
			return 1;
		}
		if (n2 > n_min) {
			return 2;
                }
	} while (n1);
return 0;
}

int main(int argc, char *argv[])
{
    double cpu_time_taken = 0;
    FILE *fp1 = NULL, *fp2 = NULL;
	unsigned int off_set = 0, result =0;
    printf("Argument count:  %d\n", argc);
    printf("File 1 is: %s\n", argv[1]);
    printf("File 2 is: %s\n", argv[2]);
    if (argc < 3)
    {
        printf("Insufficient Arguments: \n");
        printf("Help:./executable <filename1> <filename2>\n");
        return 0;
    }
    else
    {
        fp1 = fopen(argv[1],  "rb");
        if (fp1 == NULL)
        {
            printf("Error in opening file %s\n", argv[1]);
            return 0;
        }

        fp2 = fopen(argv[2], "rb");

        if (fp2 == NULL)
        {
            printf("Error in opening file %s\n", argv[2]);
            return 0;
        }

        if ((fp1 != NULL) && (fp2 != NULL))
        {
		start = clock();
		result = compare_two_binary_files(fp1, fp2, &off_set);
		end = clock();
		cpu_time_taken = ((double) (end - start)) / CLOCKS_PER_SEC;
		printf("Time taken to compare: %f\n", cpu_time_taken*1000);
 		if(result == 0)
			printf("files compare equal in content and length, fp1 size saved as offset: %d\n", off_set);
		else if(result == 1)
			printf("files differ, fp1 longer, fp2 size saved as offset:%d\n",off_set);
		else if(result == 2)
			printf("files differ, fp2 longer, fp1 size saved as offset:%d\n",off_set);
		else if(result == 3) {
			printf("files differ at (index stats from 1) offset: %d\n",off_set+1);
    			char tmp1[READSZ+1], tmp2[READSZ+1];
			size_t bytes = 0, n1 = 0, n2 =0;

			fseek(fp1, off_set, SEEK_SET);
			fseek(fp2, off_set, SEEK_SET);
        		n1 = fread (tmp1, sizeof *tmp1, sizeof tmp1 / sizeof *tmp1, fp1);
			printf("FILE1:printing %d bytes avaiable <=%d\n",n1,READSZ);
			for(int i=0; i < n1; i++){
				printf ("%x ",tmp1[i]);
			}

        		n2 = fread (tmp2, sizeof *tmp2, sizeof tmp2 / sizeof *tmp2, fp2);
			printf("\nFILE2:printing %d bytes available <=%d\n",n2,READSZ);
			for(int i=1; i < n2; i++){
				//printf ("0x%02x",i, tmp2[i]);
				printf ("%x ",i, tmp2[i]);
			}
			printf("\n");
		}
		else if(result == -1)
			printf("fp1 trouble reading.  Unspecified data in offset\n");
		else if(result == -2)
			printf("fp2 trouble reading.  Unspecified data in offset\n");
        }
    }
}
